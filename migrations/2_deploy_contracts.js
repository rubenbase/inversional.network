var ConvertLib = artifacts.require("./ConvertLib.sol");
var InverCoin = artifacts.require("./InverCoin.sol");

module.exports = function(deployer) {
  deployer.deploy(ConvertLib);
  deployer.link(ConvertLib, InverCoin);
  deployer.deploy(InverCoin);
};
