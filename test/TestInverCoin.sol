pragma solidity ^0.4.2;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/InverCoin.sol";

contract TestInverCoin{

  function testInitialBalanceUsingDeployedContract() public {
    InverCoin meta = InverCoin(DeployedAddresses.InverCoin());

    uint expected = 10000;

    Assert.equal(meta.getBalance(tx.origin), expected, "Owner should have 10000 InverCoin initially");
  }

  function testInitialBalanceWithNewInverCoin() public {
    InverCoin meta = new InverCoin();

    uint expected = 10000;

    Assert.equal(meta.getBalance(tx.origin), expected, "Owner should have 10000 InverCoin initially");
  }

}
